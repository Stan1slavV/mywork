﻿using FluentNHibernate.Mapping;

namespace MvcNhibernate.Models
{
    public class EmployeeMap : ClassMap<Employee>
    {
        public EmployeeMap()
        {
            Id(x=> x.Id);
            Map(x => x.IIN);
            Map(x => x.Surname);
            Map(x => x.Name);
            Map(x => x.Patronamyc);
            Map(x => x.DateOfBirth);
            Map(x => x.Position);
            Map(x => x.DateOfEmployee);
            Map(x => x.Leade);
            Table("Employee");
        }
    }
}