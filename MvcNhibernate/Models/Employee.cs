﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Globalization;

namespace MvcNhibernate.Models
{
    public class Employee
    {
        public virtual int Id { get; set; }
        [Required(ErrorMessage = " ИИН пользователя обязательно к заполнению")]
        [Display(Name = "ИИН")]

        public virtual long IIN { get; set; }
        [Required(ErrorMessage = " Фамилия пользователя обязательно к заполнению")]
        [Display(Name = "Фамилия")]
        public virtual string Surname { get; set; }
        [Required(ErrorMessage = " Имя пользователя обязательно к заполнению")]
        [Display(Name = "Имя")]
        public virtual string Name { get; set; }
        [Required(ErrorMessage = " Отчество пользователя обязательно к заполнению")]
        [Display(Name = "Отчество")]
        public virtual string Patronamyc { get; set; }
        [Required(ErrorMessage = " Дата рождения обязательна к заполнению")]
        [Display(Name = "Дата рождения")]
        [DataType(DataType.Date)]
        public virtual DateTime DateOfBirth { get; set; }
        [Display(Name = "Должность")]
        public virtual string Position { get; set; }
        [Required]
        [Display(Name = "Дата устройства")]
        [DataType(DataType.Date)]
        public virtual DateTime DateOfEmployee { get; set; }
        [Display(Name = "Руководитель")]
        public virtual string Leade { get; set; }

        



    }

}